package hu.myprojects.activitytracker.administration;

import lombok.Data;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Set;

@Entity
@Data
public class AppUser {

  @Id
  @GeneratedValue
  private Long id;
  private String name;
  private String code;
  private String password;
  @ElementCollection
  private Set<String> roles;

}
