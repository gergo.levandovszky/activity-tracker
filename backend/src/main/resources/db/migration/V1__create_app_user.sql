create sequence hibernate_sequence start with 1 increment by 1;

create table app_user (
   id bigint not null,
   code varchar(255),
   name varchar(255),
   password varchar(255),
   primary key (id)
);

create table app_user_roles (
   app_user_id bigint not null,
   roles varchar(255)
);

alter table app_user_roles
   add constraint FKkwxexnudtp5gmt82j0qtytnoe
   foreign key (app_user_id)
   references app_user
